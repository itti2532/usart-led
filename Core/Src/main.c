/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "SEGGER_RTT.h"
#include "string.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum
{
  APP_IDLE = 0,
  APP_TX_SENDING_COM,
  APP_RX_RECEIVING_COM,
  APP_FINISHED,
} app_state_t;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
static char r;
static char tx_buffer[100];
static uint8_t tx_buf_idx = 0;
static char rx_buffer[100];
static uint8_t rx_buf_idx = 0;
static char on[] = "on";
static char off[] = "off";
app_state_t app_state = APP_IDLE;
bool is_receiver_mode = false;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1) {
    /* USER CODE END WHILE */

     /* USER CODE BEGIN 3 */
    switch (app_state) {
      case APP_IDLE:
        SEGGER_RTT_WriteString(0, "********************* START SESSION *********************\r\n");
        SEGGER_RTT_WriteString(0, "ENTER on to ON LED or ENTER off to OFF LED\r\n");
        while(true) {
          if (HAL_GPIO_ReadPin(User_Input_GPIO_Port, User_Input_Pin) == GPIO_PIN_SET){
            is_receiver_mode = true;
            SEGGER_RTT_WriteString(0, "This device now operates in receiver mode\r\n");
            // If there is still character left in RTT input buffer, read to empty it from buffer.
            while (SEGGER_RTT_HasKey()) {
                r = SEGGER_RTT_GetKey();
                (void)r;
            }
            app_state = APP_RX_RECEIVING_COM;
            break;
          }
          if (SEGGER_RTT_HasKey()) {
            r = SEGGER_RTT_GetKey();
            tx_buffer[tx_buf_idx] = r;
            tx_buf_idx++;
            if (r == 0x0A) {
              HAL_GPIO_WritePin(User_Output_GPIO_Port, User_Output_Pin, GPIO_PIN_SET);
              is_receiver_mode = false;
              SEGGER_RTT_WriteString(0, "This device now operates in transmitter mode\r\n");
              app_state = APP_TX_SENDING_COM;
              break;
            }
          }       
        }
        break;

      case APP_TX_SENDING_COM:
        SEGGER_RTT_WriteString(0, "APP_TX_SENDING_DATA : Sending received user input\r\n");
        // Transmitting data via USART1 which user input through RTT Terminal.
        for (uint8_t i = 0; i < tx_buf_idx; i++) {
          LL_USART_TransmitData8(USART1, tx_buffer[i]);
          while(!LL_USART_IsActiveFlag_TC(USART1));
          SEGGER_RTT_PutChar(0, tx_buffer[i]);
          SEGGER_RTT_printf(0, "\n0x%x\r\n", tx_buffer[i]);
        }
        app_state = APP_FINISHED;
        break;

      case APP_RX_RECEIVING_COM:
        SEGGER_RTT_WriteString(0, "APP_RX_RECEIVING_DATA : Waiting for incoming data transmission\r\n");
        SEGGER_RTT_WriteString(0, "Incoming data transmission is : \r\n");
        // Receiving transmitted data via USART1 until finalised character which is "\n"
        while (true) {
          if (LL_USART_IsActiveFlag_RXNE(USART1)) {
            r = LL_USART_ReceiveData8(USART1);
            tx_buffer[tx_buf_idx] = r;
            tx_buf_idx++;
            if (r == 0x0A) {
              SEGGER_RTT_printf(0, "app state = %d after if\r\n", app_state);
              app_state = APP_FINISHED;
              SEGGER_RTT_Write(0, &tx_buffer, tx_buf_idx);
                for (uint8_t i = 0; i < tx_buf_idx; i++) {
                  SEGGER_RTT_printf(0, "tx_buffer[%d] = 0x%x\n", i, tx_buffer[i]);
                }
                for (uint8_t i = 0; i < tx_buf_idx; i++) {
                  if (tx_buffer[i] == '\n') {
                    tx_buffer[i] = '\0';
                  }
                }
                for (uint8_t i = 0; i < tx_buf_idx; i++) {
                  SEGGER_RTT_printf(0, "tx_buffer[%d] = 0x%x\n", i, tx_buffer[i]);
                }
                if(strcmp(tx_buffer, on) == 0) {
                  SEGGER_RTT_WriteString(0, "LED_ON\r\n");
                  HAL_GPIO_WritePin(LED_onboard_GPIO_Port, LED_onboard_Pin, GPIO_PIN_SET);
                  break;
                }
                else if (strcmp(tx_buffer, off) == 0) {
                  SEGGER_RTT_WriteString(0, "LED_OFF\r\n");
                  HAL_GPIO_WritePin(LED_onboard_GPIO_Port, LED_onboard_Pin, GPIO_PIN_RESET);
                  break;
                }
                else {
                  SEGGER_RTT_WriteString(0, "Pls Enter ONLY on OR off\r\n");
                  break;
                }
            }
          }
        }
        break;

      case APP_FINISHED:
        // Reset all status and buffer back to defaults.
        is_receiver_mode = false;
        HAL_GPIO_WritePin(User_Output_GPIO_Port, User_Output_Pin, GPIO_PIN_RESET);
        memset(tx_buffer, 0, 100);
        memset(rx_buffer, 0, 100);
        tx_buf_idx = 0;
        rx_buf_idx = 0;
        SEGGER_RTT_WriteString(0, "APP_FINISHED : Returning to IDLE State...\r\n");
        SEGGER_RTT_WriteString(0, "********************** END SESSION **********************\r\n\r\n");
        app_state = APP_IDLE;
        break;

      default:
      break;
    }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  LL_USART_InitTypeDef USART_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* Peripheral clock enable */
  LL_APB1_GRP2_EnableClock(LL_APB1_GRP2_PERIPH_USART1);

  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
  /**USART1 GPIO Configuration
  PA9   ------> USART1_TX
  PA10   ------> USART1_RX
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_9;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_10;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  USART_InitStruct.BaudRate = 115200;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  LL_USART_Init(USART1, &USART_InitStruct);
  LL_USART_DisableIT_CTS(USART1);
  LL_USART_ConfigAsyncMode(USART1);
  LL_USART_Enable(USART1);
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  LL_USART_InitTypeDef USART_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* Peripheral clock enable */
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART2);

  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
  /**USART2 GPIO Configuration
  PA2   ------> USART2_TX
  PA15   ------> USART2_RX
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_2;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_15;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_1;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  USART_InitStruct.BaudRate = 38400;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  LL_USART_Init(USART2, &USART_InitStruct);
  LL_USART_DisableIT_CTS(USART2);
  LL_USART_ConfigAsyncMode(USART2);
  LL_USART_Enable(USART2);
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_onboard_Pin|User_Output_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : LED_onboard_Pin User_Output_Pin */
  GPIO_InitStruct.Pin = LED_onboard_Pin|User_Output_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : User_Input_Pin */
  GPIO_InitStruct.Pin = User_Input_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(User_Input_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
       SEGGER_RTT_printf(0U, "ERROR : Assertion failed in file %s on line %d\r\n", file, line);
  while (true);
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
